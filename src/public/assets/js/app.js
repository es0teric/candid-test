$(document).ready(function() {

	//lets store the search results container 
	const resultsContainer = $('#search-results');
	const resultsCount = $('#results-count');

	$('select').formSelect();

	$('#filter').on('click', function() {
		$('#filter_options').fadeToggle();
	});

	//get the data on search form submit
	$('#search-form').on('submit', function(event) {
		event.preventDefault();
		//set ajax obj
		let ajaxObj = {};

		console.log($('input#search').val());

		//start the ajax object
		ajaxObj = {
			type: "POST",
			url: '/v1/get_all',
			data: {
				search: $('input#search').val(),
				end_year: $('#year').val(),
				top_num: $('#top_num').val()
			},
			beforeSend: function() {
				//empty the container and just show the loading gif
				resultsContainer.empty();
				resultsCount.empty();
				$('#loading').show();
			},
			complete: function() {
				$('#loading').hide();
			}
		};


		//do ajax call
		$.ajax(ajaxObj).done(function(data) {

			//display error when search does not work
			if( data.error ) {
				resultsContainer.append($('<h4/>').html(data.error.search[0]));
			}

			//lets display no results when the count is 0
			if( data.results === undefined || data.results.length === 0 ) {
				resultsContainer.append($('<h4/>').html('No results found, please try a different search.'));
			} else {
				resultsCount.html('<strong>Results found: ' + data.count + '</strong>');
			}

			//loop through each object in the results array
			$.each(data.results, function(key,val) {

				//lets check against the results to see if any urls are empty or null
				if( val.url ) {
					url = val.url;
				} else {
					url = '#';
				}

				//append results to the appropriate container with their appropriate elements
				resultsContainer.append(
					$('<div />', {class: 'result-item'}).append(
						$('<p />').html(val.name + '<br/>').append(
							$('<a />', {href: url, target: '_blank'}).html('Website')
							).append(
							$('<p/>').html("<span>Amount Donated: " + numeral(val.amount).format('$0,0.00') + '</span>')
							)
						)
					);
			});

		});
	});

	//grab all funders names for autocomplete
	$.post('/v1/get_all',function(data) {

		let funders = {};

		$.each(data.results, function(key, value) {
			funders[value.name] = null;
		});

		//lets setup typeahead
		$('input#search').autocomplete({
			data: funders,
			minLength: 3,
			limit: 5
		});

	});


	//tooltips to tell the user what each icon does
	tippy('#filter', {
	  content: "Click to filter",
	});


});