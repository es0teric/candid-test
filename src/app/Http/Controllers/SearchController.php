<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

//@return \Illuminate\Http\Response

class SearchController extends Controller
{

    /**
     * Index page for search functionality
     * 
     * @return void
     */
    public function index() {  

        return view('search', [
            'item' => 'New item',
            'page_title' => 'Search',
            'starting_year' => date('Y') - 20,
            'current_year' => (int) date('Y')
        ]);

    }

    /**
     * Gets all results according to query strings
     * 
     * @return \Illuminate\Http\Response
     */
    public function get_all(Request $request) {

        //grab all request inputs
        $search = $request->input('search');
        $top_num = $request->input('top_num');
        $end_year = $request->input('end_year');

        $validator = Validator::make(
            [
                'search' => $search,
                'end_year' => $end_year,
                'top_num' => $top_num
            ], 
            [
                'search' => 'nullable|regex:/^[\pL\s\-]+$/u',
                'end_year' => 'nullable|numeric',
                'top_num'  => 'nullable|numeric'
            ]
        );
        
        $retval = [];

        if( !$validator->fails() ) {

            //now lets check if the user has an empty search query
            $retval = $this->api_query($search, $end_year, $top_num);

        } else {
            $retval['error'] = $validator->messages();
        }

        return response()->json( $retval );

    }

    /**
     * Query against API and return results against 
     * 
     * @param  string $query     search to query against
     * @param  string $end_year  ending year for search query
     * @param  string $top       top amount of results to show
     * 
     * @return \Illuminate\Http\Response JSON response
     */
    protected function api_query( $query = false, $end_year = false, $top = false ) {

        //get api key from envvar
        $query_var = '?apiKey=' . getenv('CANDID_API_KEY');
        $retval = [];

        //lets check if end_year was defined, if not lets use the current year
        if( !empty( $end_year ) ) {
            $query_var .= '&end_year=' . $end_year;
        } else {
            $query_var .= '&end_year=' . date('Y');
        }

        //lets check if top was defined if not, lets make a query gets all results
        if( !empty( $top ) ) {
            $query_var .= '&top=' . $top;
        } else {
            $query_var .= '&top=30000';
        }

        //use guzzle to send a get request
        $client = new Client();
        $response = $client->request('GET', 'https://maps.foundationcenter.org/api/hip/getTopFunders.php' . $query_var);

        //store main data in body var to check against
        $body = json_decode( $response->getBody() )->data;

        //lets check if the query param is empty
        if( !empty( $query ) ) {

             $count = 1;

            //loop through all api results to find all names related to search query
            foreach( $body->results->rows as $result ) {

                //lets check if any names of results return with 
                if( strpos( $result->name, $query ) !== false ) {

                    //lets grab the data and output the correct count since API 
                    //does not output the correct result count
                    $retval['count'] = $count++;
                    $retval['results'][] = $result;
                    
                }

            }

        } else {

            //lets grab the data and output the correct count since API 
            //does not output the correct result count
            $retval = [
                'count' => count( $body->results->rows ),
                'results' => $body->results->rows
            ];
        }

        return $retval;

    }
}
