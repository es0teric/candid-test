<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
        <title>Candid App - {{ $page_title }}</title>
        <link href="{{ url('assets/materialize/css/materialize.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
        <link href="{{ url('assets/css/app.css') }}" type="text/css" rel="stylesheet" media="screen,projection" />
    </head>

    <body>
        <nav class="light-blue lighten-1" role="navigation">
            <div class="nav-wrapper container">
                <a id="logo-container" href="#" class="brand-logo">Candid App</a>

                <ul class="right hide-on-med-and-down">
                    <li><a href="#">Search</a></li>
                </ul> <!-- /.right.hide-on-med-and-down -->

                <ul id="nav-mobile" class="sidenav">
                    <li><a href="#">Search</a></li>
                </ul> <!-- #nav-mobile.sidenav -->

            </div> <!-- /.nav-wrapper.container -->
        </nav> <!-- /.light-blue-lighten-1 -->

        <div class="section search-wrapper">
            <div class="container">
                <form id="search-form" action="/v1/get_all" method="get">
                  <div class="row" id="search">
                    <div class="col s12">
                      <div class="input-field col s12">
                        <a href="#"><i class="material-icons prefix" id="filter">filter_list</i></a>
                        <input placeholder="Funder's name search" id="search" type="text" name="search" class="validate" />
                        <i class="material-icons prefix">search</i>
                      </div> <!-- /.input-field.col.s6 -->
                    </div> <!-- /.col.s12 -->
                  </div> <!-- /.row -->

                  <div class="row" id="filter_options">
                    <div class="input-field col s3 offset-s3">
                        <!--<a href="#"><i class="material-icons prefix date_range">date_range</i></a>-->
                        <select id="year">
                          <option value="" disabled selected>Select Year</option>
                          <?php $i = 0; ?>
                            @for( $i = $starting_year; $i <= $current_year; $i++ )
                            <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                    </div> <!-- /.col.s3 -->
                    <div class="input-field col s3">
                        <!--<a href="#"><i class="material-icons prefix date_range">date_range</i></a>-->
                        <input placeholder="Top amount to show" id="top_num" name="top_num" type="number" class="validate" />
                    </div> <!-- /.col.s3 -->
                  </div> <!-- /.row -->
                  <div class="row center">
                    <button class="btn waves-effect waves-light light-blue lighten-1" type="submit">Submit</button>
                  </div> <!-- /.row -->
                </form> <!-- /#search-form -->
            </div> <!-- /.container -->
            <div class="section no-pad-bot" id="index-banner">
                <div class="container">
                    <div class="row center">
                        <img id="loading" src="{{ url('assets/img/loading.gif') }}" width="120" height="120" />
                    </div> <!-- /.row.center -->
                    <div class="row center">
                        <div id="results-count"></div> <!-- /.results-count -->
                    </div> <!-- /.row.center -->
                    <div class="row center" id="search-results">
                    </div> <!-- /.row.center#search-results -->
                </div> <!-- /.container -->
                <div class="container">
                    @yield('content')
                </div> <!-- /.container -->
            </div> <!-- /#index-banner.section.no-pad-bot -->
        </div> <!-- /.section.search-wrapper -->

        <footer class="page-footer">
            <div class="footer-copyright">
              <div class="container">
                Made by <a class="orange-text text-lighten-3" href="https://bitbucket.org/es0teric">G. Leo Fulgencio</a>
              </div>
            </div> <!-- /.footer-copyright -->
        </footer> <!-- /.page-footer -->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
        <script src="{{ url('assets/materialize/js/materialize.min.js') }}"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
        <script src="https://unpkg.com/@popperjs/core@2"></script>
        <script src="https://unpkg.com/tippy.js@6"></script>
        <script src="{{ url('assets/js/app.js') }}"></script>

    </body>
</html>



