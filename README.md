**Installation instructions**
---
In the directory that this was installed, run:   
`docker-compose up --build`   

Then add a .env file in src/ with the following envvar:   
`CANDID_API_KEY=<insert key here>`   

And run the app on the url:   
`http://localhost:8080/search`


It should run fine after that, there is no database it just runs off the API with a few input validation checks.